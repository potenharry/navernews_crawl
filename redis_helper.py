import redis


def connect_db():
    try:
        conn = redis.StrictRedis(host='192.168.0.57', port=6379, db=14)

    except Exception as e:
        print("DB connection error: ", e)

    return conn


def set_key(main_key):  # 종목별로 article_id key 생성 , format: article_id_data:A080564:article_id
    conn = connect_db()
    try:
        conn.set(main_key, 1)
        conn.expire(main_key, 60 * 60 * 24 * 10)  # TODO expire time setting
    except Exception as e:
        print("set Error:", e)


def check_exist_key(main_key):  # article_id 존재하는 지 확인
    conn = connect_db()
    return conn.exists(main_key)  # 1 / 0

# set_key('article_id_data:A080564:0000')

# print(check_exist_key('article_id_data:A080564:000120')) 1 or 0



