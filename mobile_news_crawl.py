# -*- coding: utf-8 -*-
from urllib.request import urlopen, Request
import re
import bs4

flag_url = 'https://finance.naver.com'
target_url = 'https://m.stock.naver.com/item/main.nhn#/stocks/180640/news'

path = '/Users/harry/Downloads/chromedriver'


def crawl_mobile_stock_news(url):
    '''
      crawl_mobile_stock_news : 각 종목별 모바일 뉴스, 공시 크롤링
      한 페이지의 리스트 가져오기
    '''
    req = Request(url)
    page = urlopen(req)
    html = page.read()
    soup = bs4.BeautifulSoup(html, features='lxml')
    # print(soup)
    stock_news_list = soup.find('div', class_='_news_list_contents')
    print(stock_news_list)
    # return stock_news_list


crawl_mobile_stock_news(target_url)

