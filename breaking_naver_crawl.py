# -*- coding: utf-8 -*-
from urllib.request import urlopen, Request
import re
import bs4


def crawl_breaking_news(url):
    '''
      crawl_breaking_news : 네이버 금융 뉴스 -> 실시간 속보 기사 크롤링
      한 페이지의 리스트 가져오기
    '''
    req = Request(url)
    page = urlopen(req)
    html = page.read()
    soup = bs4.BeautifulSoup(html, features='lxml')

    news_section = soup.find('ul', class_='realtimeNewsList').findAll('li', class_='newsList')
    return news_section  # 상단기사(news_section[0]), 하단기사(news_section[1])로 구성


def get_crawl_info(news_section):
    # news_section = crawl_breaking_news(target_url)

    a_tag_set_top = news_section[0].findAll('a')  # 상단 10개 기사
    a_tag_set_bottom = news_section[1].findAll('a')  # 하단 10개 기사
    a_tag_set = a_tag_set_top + a_tag_set_bottom

    values = []
    for a_tag in a_tag_set:  # title과 url 추출
        if a_tag.get('title'):

            temp = {'url': flag_url + str(a_tag.get('href')), 'title': str(a_tag.get('title'))}
            values.append(temp)

    values = get_content(values)  # content 추가하여 return
    return values


def get_content(values):
    for value in values:
        url = value['url']
        req = Request(url)
        page = urlopen(req)
        html = page.read()
        soup = bs4.BeautifulSoup(html, features='lxml')
        content = soup.find('div', class_='articleCont').text
        content = preprocess_content(content)
        temp = {'content': content}
        stock = get_stock_name(content)
        if stock is not False:
            temp.update({'stock': stock})
        value.update(temp)
    print(values)
    return values


def preprocess_content(content):  # 전처리
    content = re.sub('\\xa0', '', content)
    content = re.sub('\\n', '', content)
    content = re.sub('\\\\xa0', '', content)
    content = re.sub('\\\\n', '', content)
    content = re.sub('[\{\}\\/?.,;:|\)*~`!^\-_+<>@#$%&\\\=\(\'\")]', '', content)
    content = ' '.join(content.split())
    idx = content.find('▶')  # TODO 하드코딩 말고 더 정형화된 패턴 이용하여 처리
    content = content[:idx]
    return content


def get_stock_name(content):
    '''
    종목 찾기 함수
    패턴 : 코오롱002020 or KNN058400
    :param content: 기사 본문
    :return: 페턴에 맞는것이 있다면 종목, 없다면 false
    '''
    regex = re.compile('[가-힝a-zA-Z]+[0-9]{6}')
    mc = regex.findall(content)
    regex = re.compile('[가-힝a-zA-Z]+[0-9]+[가-힝a-zA-Z]+[0-9]{6}')
    temp = regex.findall(content)
    mc = mc + temp
    if mc:
        return mc
    else:
        return False


def check_next_page(url):
    req = Request(url)
    page = urlopen(req)
    html = page.read()
    soup = bs4.BeautifulSoup(html, features='lxml')
    paging = soup.find('table', class_='Nnavi').find('table').find('tr').findAll('td')
    on_page = None

    for idx, page in enumerate(paging):
        if page.get('class') and page.get('class')[0] == 'on':
            on_page = idx

            if on_page == len(paging) - 1:
                print('마지막 페이지입니다.')
                return False

        if on_page is not None and idx == on_page + 1:
            next_page_url = page.find('a').get('href')
            return next_page_url


if __name__ == '__main__':
    target_url = 'https://finance.naver.com/news/news_list.nhn?mode=LSS2D&section_id=101&section_id2=258'
    flag_url = 'https://finance.naver.com/'  # href에서 가져오는 url 과 더함
    cnt = 0

    while True:
        if cnt == 11:
            break  # 우선은 한번
        news_section = crawl_breaking_news(target_url)
        result = get_crawl_info(news_section)
        print(result)
        next_page_url = check_next_page(target_url)

        if next_page_url is not False:
            target_url = flag_url + next_page_url
        else:  # 만약 False라면 break
            break
        cnt += 1
