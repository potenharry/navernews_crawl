# -*- coding: utf-8 -*-
from urllib.request import urlopen, Request
from multiprocessing import Process, current_process, Pool
from pathlib import Path
from ast import literal_eval
import redis_helper as rh
import re
import bs4
import sys
import time
import db_helper as dh

start_time = time.time()

flag_url = 'https://finance.naver.com'
# headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36
# (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'}


def crawl_stock_news(url, cnt):
    '''
      crawl_stock_news : 각 종목별 뉴스, 공시 크롤링
      한 페이지의 리스트 가져오기
    '''
    if cnt == 0:
        req = Request(url)
        page = urlopen(req)
        html = page.read()
        soup = bs4.BeautifulSoup(html, features='lxml')

        # iframe에서 실제 url 추출
        get_real_src = soup.find('iframe', id='news_frame').get('src')
        url = flag_url + get_real_src

    req = Request(url)
    page = urlopen(req)
    html = page.read()
    soup = bs4.BeautifulSoup(html, features='lxml')
    try:
        stock_news_list = soup.find('div', class_='tb_cont').find('tbody').findAll('tr')
    except:
        print('dont haver news list')
        return False
    return stock_news_list, url


def process_news_list(news_list, stock_code):
    # news_list = crawl_stock_news(target_url + stock_code)
    values = []
    before_class_set = []
    for news in news_list:
        class_set = news.get('class')
        print(class_set)
        if class_set and ('hide_news' in class_set or 'relation_lst' in class_set):
            print('관련기사는 crawling 하지 않습니다.')
            before_class_set = class_set
            continue
        if before_class_set and 'relation_lst' in before_class_set:
            print('관련기사는 crawling 하지 않습니다.')
            before_class_set = class_set
            continue
        before_class_set = class_set
        try:
            td = news.find('td', class_='title')
            info = news.find('td', class_='info').text
            date = news.find('td', class_='date').text
            date = date.strip()
        except:
            print('dont have any text')
            return False
        a = td.find('a')
        temp_url = a.get('href')
        article_id = temp_url[temp_url.find('article_id'):temp_url.find('&office')].replace('article_id=', '')
        print('stock_code : ', stock_code, ' article_id: ', article_id)
        main_key = 'news_id_data:' + stock_code + ':' + article_id
        if rh.check_exist_key(main_key) == 0:
            title = a.text.replace('"', '\"')  # title도 preprocess
            temp = {'url': flag_url + a.get('href'), 'title': title, 'news_id': article_id, 'source': info, 'issue_date': date}
            value_temp = values.append(temp)
            values = get_content(values)
    return values


def get_content(values):
    for value in values:
        url = value['url']
        req = Request(url)
        page = urlopen(req)
        html = page.read()
        soup = bs4.BeautifulSoup(html, features='lxml')
        try:
            content = soup.find('table', class_='view').text
            content = preprocess_content(content)
        except:
            print('article content is none')
            temp = {'content': ''}
            value.update(temp)
            return values

        temp = {'content': content}
        stock = get_stock_name(content)
        if stock is not False:
            temp.update({'stock': stock})
        value.update(temp)
    return values


def preprocess_content(content):  # 전처리
    content = re.sub('\\xa0', ' ', content)
    content = re.sub('\\n', ' ', content)
    content = re.sub('\\\\xa0', ' ', content)
    content = re.sub('\\\\n', ' ', content)
    content = re.sub('[\{\}\\/?.,;:|\)*~`!^\-_+<>@#$%&\\\=\(\'\")]', ' ', content)
    content = ' '.join(content.split())
    content = content.replace('"', '\"')
    content = content.replace('"', '\"')

    idx = content.find('▶')  # TODO 하드코딩 말고 더 정형화된 패턴 이용하여 처리
    content = content[:idx]
    return content


def get_stock_name(content):
    '''
    종목 찾기 함수
    패턴 : 코오롱002020 or KNN058400
    :param content: 기사 본문
    :return: 페턴에 맞는것이 있다면 종목, 없다면 false
    '''
    regex = re.compile('[가-힝a-zA-Z]+[0-9]{6}')
    mc = regex.findall(content)
    regex = re.compile('[가-힝a-zA-Z]+[0-9]+[가-힝a-zA-Z]+[0-9]{6}')
    temp = regex.findall(content)
    mc = mc + temp
    if mc:
        return mc
    else:
        return False


def check_next_page(url):
    req = Request(url)
    page = urlopen(req)
    html = page.read()
    soup = bs4.BeautifulSoup(html, features='lxml')
    paging = soup.find('table', class_='Nnavi').find('tr').findAll('td')
    on_page = None

    for idx, page in enumerate(paging):
        if page.get('class') and page.get('class')[0] == 'on':
            on_page = idx

            if on_page == len(paging) - 1:
                print('마지막 페이지입니다.')
                return False

        if on_page is not None and idx == on_page + 1:
            next_page_url = page.find('a').get('href')
            return next_page_url


def main(stock_code):
    # stock_code = sys.argv[1]  # format: python3 stock_news_crawl.py '005930'
    # stock_code = '180640'
    target_url = 'https://finance.naver.com/item/news.nhn?code='
    target_url = target_url + stock_code
    cnt = 0
    print('main start')
    while True:
        if cnt == 1:
            break  # 10페이지까지 크롤링
        print(stock_code, 'cnt: ', cnt)
        stock_code = 'A' + stock_code
        news_list, target_url = crawl_stock_news(target_url, cnt)
        if news_list is False:
            break
        result = process_news_list(news_list, stock_code)
        if result is False or not result:
            print('hereeee?')
            break

        # with open(stock_code + '.txt', 'w') as f:
        #     for data in result:
        #         f.write(str(data) + '\n')
        print('here')
        db = dh.get_connect()
        dh.get_sql(db, stock_code, result)
        db.close()

        cnt += 1


if __name__ == '__main__':
    '''

    # 기존 코드
    # stock_code = sys.argv[1]  # format: python3 stock_news_crawl.py '005930'
    stock_code = '034120'
    target_url = 'https://finance.naver.com/item/news.nhn?code='
    target_url = target_url + stock_code
    cnt = 0
    main(stock_code)
    # while True:
    #     if cnt == 1:
    #         break  # 10페이지까지 크롤링
    #     print(stock_code, 'cnt: ', cnt)
    #     stock_code = 'A' + stock_code
    #     news_list, target_url = crawl_stock_news(target_url, cnt)
    #     result = process_news_list(news_list, stock_code)
    #
    #     with open(stock_code + '.txt', 'w') as f:
    #         for data in result:
    #             f.write(str(data) + '\n')
    #
    #     cnt += 1
    '''
    '''
        프로세스 추가 코드 
    '''
    stocks = dh.get_stock_code()
    # exist_stocks = dh.get_stock_code_pn()  # 전체 종목 크롤링 중간 에러가 났을때만 사용
    procs = []
    stock_set = []
    print('start')
    for idx, stock in enumerate(stocks):
        print('stock: ', stock, ' start !')
        stock = stock[0][1:]  # abstract stock_code in tuple and remove 'A'

        # if 'A' + stock in exist_stocks:
        #     print('here')
        #     continue
        stock_set.append(stock)
        if len(stock_set) == 30 or idx == len(stocks) - 1:
            pool = Pool(4)
            pool.map(main, stock_set)
            stock_set = []

    print("--- %s seconds ---" % (time.time() - start_time))  # --- 245.4546821117401 seconds ---

