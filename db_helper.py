# -*- coding: utf-8 -*-
from ast import literal_eval
import MySQLdb
import db_config
import redis_helper as rh
import sys
import time


db_infos = {
    'db1': {'host': db_config.db1['host'], 'port': db_config.db1['port'],
            'user': db_config.db1['user'], 'passwd': db_config.db1['passwd'], 'db': db_config.db1['db']},
    'db2': {'host': db_config.db2['host'], 'port': db_config.db2['port'],
            'user': db_config.db2['user'], 'passwd': db_config.db2['passwd'], 'db': db_config.db2['db']},
}

artice_table = 'article_set'


def get_connect():
    db_info = db_infos['db1']
    db = MySQLdb.connect(host=db_info['host'], port=db_info['port'],
                         user=db_info['user'], passwd=db_info['passwd'], db=db_info['db'], charset='utf8')

    db.autocommit(True)
    return db


def get_sql(db, stock_code, result):
    sql = ''
    # with open(stock_code + '.txt', 'r') as f:
    #
    #     while True:
    #         data = f.readline()
    #         if not data:
    #             break
    #         print(data)
    #         data = literal_eval(data)
    #         sql, main_key, data_set = make_query(data, stock_code)
    #         execute_sql(db, sql, main_key, data_set)
    for data in result:
        if not data:
            break
        print(data, 'Type: ', str(type(data)))
        # data = literal_eval(data) # string 이 아니라 애초에 dict type 임
        sql, main_key, data_set = make_query(data, stock_code)
        execute_sql(db, sql, main_key, data_set)
        '''
        data = f.readline()
        print(data)
        data = literal_eval(data)
        sql += make_query(data, stock_code)
        '''

    return 1


def make_query(data, stock_code):
    # url = 'hh'
    # title = 'hh'

    sql = "INSERT INTO article_set (code, url, title, content, news_id, source, issue_date) " \
          "VALUES (%s, %s, %s, %s, %s, %s, %s)"
    sql += " ON DUPLICATE KEY UPDATE "\
           "code=%s, url=%s, title=%s, content=%s, news_id=%s, source=%s, issue_date=%s"
    data_set = (stock_code, data['url'], data['title'], data['content'], data['news_id'], data['source'], data['issue_date'],
                stock_code, data['url'], data['title'], data['content'], data['news_id'], data['source'], data['issue_date'])

    # set article id
    main_key = 'news_id_data:' + stock_code + ':' + data['news_id']

    return sql, main_key, data_set


def execute_sql(db, sql, main_key, data_set):
    """
        Update 1m data (from get_ohlc_sql function) in MySQL
    """
    if len(sql) > 0 and rh.check_exist_key(main_key) != 1:
        try:
            print(sql)
            cursor = db.cursor()
            cursor.execute(sql, data_set)
            rh.set_key(main_key)
            print('success')
        except Exception as e:
            print(e)
            return False
    return True


def get_stock_code():
    db_info = db_infos['db2']
    db = MySQLdb.connect(host=db_info['host'], port=db_info['port'],
                         user=db_info['user'], passwd=db_info['passwd'], db=db_info['db'], charset='utf8')
    cursor = db.cursor()
    sql = 'select distinct(code) from master_data where date=20190617;' # TODO date를 cur date 로 변환
    print('sql:', sql)
    cursor.execute(sql)
    result = cursor.fetchall()
    # print(result)
    # return result
    # for stock in result:
    #     print(stock[0][1:])
    return result


def get_stock_code_pn():
    db_info = db_infos['db1']
    db = MySQLdb.connect(host=db_info['host'], port=db_info['port'],
                         user=db_info['user'], passwd=db_info['passwd'], db=db_info['db'], charset='utf8')
    cursor = db.cursor()
    sql = 'select distinct(code) from article_set;' # TODO date를 cur date 로 변환
    print('sql:', sql)
    cursor.execute(sql)
    result = [item[0] for item in cursor.fetchall()]
    return result

# result= get_stock_code_pn()

